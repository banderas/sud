$(function(){
/* first_linkmenu */
    $('.first_linkmenu > a').click(function(e){
      e.preventDefault();
      $('.dropdown_menu').slideToggle();
      $(this).toggleClass('active');
    });
/*burger menu */
    $('.burger').click(function() {
        $(this).toggleClass('rotate');
    });
/* header_menu */
    $('.dropdown_menu a').click(function(e) {
        $('.dropdown_menu').slideToggle();
        $('.dropdown_menu a.active').removeClass('active');
        $(this).addClass('active');
        $('.first_linkmenu > a').toggleClass('active');
        $('.first_linkmenu_selected').html('<br>' + $(this).html());
        $('.header_wrapper .wrapper_topmenu .first_linkmenu > a').height(60);
        e.preventDefault();
    });
//customize select
    $(".custom-select").selectmenu({
        change: function( event, ui ) {
            $(this).addClass('changed');
        },
        open: function(){
            $(this).parent().find('.ui-selectmenu-button').removeClass('err');
        }
    });
//text area reesize
    var textarea = document.querySelector('.resize_text');
//var textarea = document.getElementsByClassName('resize_text');
    if (textarea){
        textarea.addEventListener('keydown', autosize);
    }

    function autosize(){
        var el = this;
        setTimeout(function(){
            el.style.cssText = 'height:auto; padding:0';
            // for box-sizing other than "content-box" use:
            // el.style.cssText = '-moz-box-sizing:content-box';
            var h = parseInt(el.scrollHeight) + 14;
            el.style.cssText = 'height:' + h + 'px';
        },0);
    }
/*autocomplete_field*/
    var names = [ 'Jorn Zaefferer', 'Scott Gonzalez', 'John Resig', 'Amb Troisky' ];
    var accentMap = {
      'a': 'a',
      'o': 'o'
    };
    var normalize = function( term ) {
      var ret = '';
      for ( var i = 0; i < term.length; i++ ) {
        ret += accentMap[ term.charAt(i) ] || term.charAt(i);
      }
      return ret;
    };
    $('.autocomplete_input').autocomplete({
      source: function( request, response ) {
        var matcher = new RegExp( $.ui.autocomplete.escapeRegex( request.term ), 'i' );
        response( $.grep( names, function( value ) {
            value = value.label || value.value || value;
            return matcher.test( value ) || matcher.test( normalize( value ) );
        }));
      },
      open: function( event, ui ) {
        $(this).addClass('open');
      },
      close: function( event, ui ) {
        $(this).removeClass('open');
      }
    });
/*upload files */  
    var countDopInpFile = 20;
    $('body').on('click','.add_file_button',function(e){
        e.preventDefault();
        $('.wrapper_uploadfileslist').append('<div class="upload-holder" style="display: none;"><div class="left_item">'+
            '<div class="fild_wrapper ">'+
                '<label>Название документа</label>'+
                '<div class="input_wrap">'+
                    '<input type="text" required class="doc-name" name="name'+countDopInpFile+'">'+
                    '<span class="icon"></span>'+
                '</div>'+
          '</div>'+
        '</div>'+
        '<div class="right_item">'+
            '<div class="wrapper_uploadfile fild_wrapper disabled">'+
                '<input type="file" class="file_input">'+
                    '<a href="#" class="upload last">Удалить</a>'+
                    '<div class="input_wrap">'+
                    '<input type="text" class="file_name" required disabled name="name'+(countDopInpFile+1)+'">'+
                    '<span class="icon"></span>'+
                    '</div>'+
                '</div>'+
            '</div>'+
            '<div class="clear"></div></div>');
        countDopInpFile++;
        $('.upload.last').removeClass('last').click();
    });

    var inp = $('.wrapper_uploadfileslist .file_input,.wrapper_uploadfileslistzpo .file_input');

    var uploadFileInput = $('.wrapper_uploadfileslist .file_name,.wrapper_uploadfileslistzpo .file_name');
    var docName = $('.wrapper_uploadfileslist .doc-name,.wrapper_uploadfileslistzpo .doc-name');
    $('.wrapper_uploadfileslist,.wrapper_uploadfileslistzpo').on('click','.upload',function(e){
        e.preventDefault();
        inp = $(this).parents('.wrapper_uploadfile').find('.file_input');
        uploadFileInput = $(this).parents('.wrapper_uploadfile').find('.file_name');
        docName = $(this).parents('.upload-holder').find('.doc-name');
        if (!$(this).hasClass('remove')) {
            $(this).parents('.wrapper_uploadfile').find('.file_input').click();
        } else {
            $(this).removeClass('remove').text('Удалить');
            inp.val('');
            uploadFileInput.val('');
            $(this).parents('.upload-holder').remove();
        }
    });
    var fileApi = ( window.File && window.FileReader && window.FileList && window.Blob ) ? true : false;

    $('.wrapper_uploadfileslist,.wrapper_uploadfileslistzpo').on('change','input[type="file"]',function(){
        var fileName;
        docName = $(this).parents('.upload-holder').find('.doc-name');
        if( fileApi && inp[ 0 ].files[ 0 ] ) {
            fileName = inp[ 0 ].files[ 0 ].name;
        } else {
            fileName = inp.val().replace( 'C:\\fakepath\\', '' );
        }
        if( ! fileName.length ) {
                return;
        }
        uploadFileInput.val( fileName );

        var pos = fileName.lastIndexOf('.');
        if (!pos) {
            fileName = fileName;
        }
        fileName = fileName.slice(0,pos);
        docName.val( fileName );

        $(this).parents('.upload-holder').find('.upload').addClass('remove').text('Удалить');
        $(this).parents('.upload-holder').fadeIn(300);
    }).change();

/*  datepicker  */
    $.datepicker.regional['ru'] = {
        closeText: 'Çàêðûòü',
        prevText: '&#x3c;Ïðåä',
        nextText: 'Ñëåä&#x3e;',
        currentText: 'Ñåãîäíÿ',
        monthNames: ['Январь','Февраль','Март','Апрель','Май','Июнь',
        'Июль','Август','Сентябрь','Октябрь','Ноябрь','Декабрь'],
        monthNamesShort: ['Янв','Фев','Мар','Апр','Май','Июн',
        'Июл','Авг','Сен','Окт','Ноя','Дек'],
        dayNames: ['воскресенье', 'понедельник', 'вторник','среда','четверг','пятница','суббота'],
        dayNamesShort: ['Вос', 'Пон', 'Вто','Сре','Чет','Пят','Суб'],
        dayNamesMin: ['Вс', 'Пн','Вт','Ср','Чт','Пт','Сб'],
        dateFormat: 'dd.mm.yy',
        firstDay: 1,
        isRTL: false
    };
    $.datepicker.setDefaults($.datepicker.regional['ru']);
    $.datepicker._updateDatepicker_original = $.datepicker._updateDatepicker;
      $.datepicker._updateDatepicker = function(inst) {
        $.datepicker._updateDatepicker_original(inst);
        var afterShow = this._get(inst, 'afterShow');
        if (afterShow)
      afterShow.apply((inst.input ? inst.input[0] : null));  // trigger custom callback
    }
    $('#form1').validate({
        rules: {
            left_ddatepicker: {
                required: true,
                date: true
            },
            right_datepicker: {
                required: true,
                date: true
            }
        }
    })
    $('.datepiker').datepicker({
        changeMonth: true,
        changeYear: true,
        yearRange: '1996:2066',
        regional: 'ru',
        showOtherMonths: true,
        selectOtherMonths: true,
        showOn: 'both',
        buttonImage: '../_i/calendar20x20.png',
        buttonImageOnly: true,
        showButtonPanel: false,
        buttonText: 'Выберите дату',
        minDate: new Date(1996, 0, 1),
        maxDate: new Date(2066, 12, 30),
        afterShow: function(input, inst) { 
          $('.ui-datepicker-month,.ui-datepicker-year').selectbox({
            onOpen: function (inst) {
              $(this).parent().find('.sbSelector').addClass('sbSelectorOpen');
            },
            onClose: function (inst) {
              $(this).parent().find('.sbSelector').removeClass('sbSelectorOpen');
            }
          });
        }, 
        beforeShow: function (input, inst) {
            $('.datepicker').hide();
            setTimeout(function () {
                var offset2 = inst.input.offset();
                var offset = inst.dpDiv.offset();
                if (offset2.top >= offset.top) {
                    inst.dpDiv.css({
                        top: (offset.top - 10)
                    });
                } else {
                    inst.dpDiv.css({
                        top: (offset.top + 10)
                    });
                }
            }, 300);
        }
    });

    $(document).on('click', function(e) { 
        var ele = $(e.toElement); 
       
        var container = $('#ui-datepicker-div');
        var container2 = $('.wrapper_datepiker');
        var container3 = $('.ui-datepicker .ui-datepicker-header .ui-datepicker-title .sbOptions li ');
        if ((container.has(e.target).length === 0) && (container2.has(e.target).length === 0) && (!$(ele).parents('.sbOptions').length)){
            $('.datepiker').datepicker('hide');
        }
    });

/* checkboxes */
    $('input[type="checkbox"],input[type="radio"]').iCheck({
        checkboxClass: 'checkbox_custom',
        radioClass: 'radiobox_custom',
      });
      $('.checkbox_custom, .radiobox_custom').each(function(){
       if ($(this).hasClass('disabled')) {
        $(this).parents('.item_checkradio').addClass('disabled');
      }
    });

    $('.item_checkradio input').on('ifChecked', function(event){
        if ($(this).parents('.item_checkradio').hasClass('level1')){
            $(this).parents('.ui-accordion-content').find('.inner_line').slideUp();
            $('.reddash_block').slideUp();
            $(this).parents('.line_calc').find('.inner_line').slideDown();
        } else if ($(this).parents('.item_checkradio').hasClass('level2')) {
            $(this).parents('.inner_line').find('.filedtext').slideUp();
            $('.reddash_block').slideUp();
            $(this).parents('.inner_linelevel2').find('.filedtext').slideDown();
        }
    });

/*switcher */
    $('.wrapper_switcher .active').each(function(){
        if ( $(this).hasClass('simple')){
          $(this).parents('.wrapper_switcher').find('.switcher_btn').css({
            'margin-left': '0px'
          });
        } else {
          $(this).parents('.wrapper_switcher').find('.switcher_btn').css({
            'margin-left': '15px'
          });
        }
      });

      $('.wrapper_switcher a').click(function(e){
        e.preventDefault();
        if (!$(this).hasClass('active')) {
         $(this).parents('.wrapper_switcher').find('a').removeClass('active');
         $(this).addClass('active');

         if ($(this).hasClass('simple')){
          $(this).parents('.wrapper_switcher').find('.switcher_btn').css({
            'margin-left': '0px'
          });
          $('.extended-form').fadeOut();
            $('.simple-form').fadeIn();
        } else {
          $(this).parents('.wrapper_switcher').find('.switcher_btn').css({
            'margin-left': '15px'
          });
          $('.extended-form').fadeIn();
        $('.simple-form').fadeOut();
        }
      } 
    });
      $('.wrapper_switcher .switcher').click(function(){
       if ($(this).parents('.wrapper_switcher').find('.active').hasClass('simple')){
        $(this).find('.switcher_btn').css({
          'margin-left': '15px'
        });
        $(this).parents('.wrapper_switcher').find('a').removeClass('active');
        $(this).parents('.wrapper_switcher').find('.extended').addClass('active');
        $('.extended-form').fadeIn();
        $('.simple-form').fadeOut();
      } else {
        $(this).find('.switcher_btn').css({
          'margin-left': '0px'
        });
        $(this).parents('.wrapper_switcher').find('a').removeClass('active');
        $(this).parents('.wrapper_switcher').find('.simple').addClass('active');
        $('.extended-form').fadeOut();
        $('.simple-form').fadeIn();
        
      }
    });

/*tooltip */
    $('.tooltip').tooltip({
        position: {
            my: "center bottom-20",
            at: "center top",
            using: function( position, feedback ) {
                $( this ).addClass('custom-tooltip');
                $( this ).css( position );
                $( "<div>" )
                    .addClass( "arrow" )
                    .addClass( feedback.vertical )
                    .addClass( feedback.horizontal )
                    .appendTo( this );
            }
          }
    });

/*accordion */
/*
    $('.wrapper_accordion .ui-accordion-header').each(function(){
        $(this).append('<span class="ui-accordion-header-icon"></span>');
        if ($(this).hasClass('ui-state-active')) {
            $(this).parent().find('>.ui-accordion-content').show();
        }
    });
    $('.wrapper_accordion').on('click',' .ui-accordion-header',function(e){
        e.preventDefault();
        $(this).toggleClass('ui-state-active');
        $(this).parent().find('.ui-accordion-content').slideToggle('medium');
    });
*/

/*avaible cases */
    $('.jsbtn_dash').on('click',function(e){
        e.preventDefault();
        $(this).toggleClass('open');
        $(this).parent().find('.wrap_case').slideToggle();
    });
/*tabs */
    /*$( ".tabs_wrapper" ).tabs();
    //for btn dashed
    $('.jsbtn_dash').on('click',function(e){
      e.preventDefault();
      $(this).toggleClass('open');
      $(this).parent().find('.wrap_case').slideToggle();
    });*/
});
//form validator
jQuery.extend(jQuery.validator.messages, {
    required: "Это поле необходимо заполнить!",
    remote: "Please fix this field.",
    email: "Please enter a valid email address.",
    url: "Please enter a valid URL.",
    date: "Please enter a valid date.",
    dateISO: "Please enter a valid date (ISO).",
    number: "Please enter a valid number.",
    digits: "Please enter only digits.",
    creditcard: "Please enter a valid credit card number.",
    equalTo: "Please enter the same value again.",
    accept: "Please enter a value with a valid extension.",
    maxlength: jQuery.validator.format("Please enter no more than {0} characters."),
    minlength: jQuery.validator.format("Please enter at least {0} characters."),
    rangelength: jQuery.validator.format("Please enter a value between {0} and {1} characters long."),
    range: jQuery.validator.format("Please enter a value between {0} and {1}."),
    max: jQuery.validator.format("Please enter a value less than or equal to {0}."),
    min: jQuery.validator.format("Please enter a value greater than or equal to {0}.")
});
function dateValidation(dateString){
    var m = dateString.match(/^(\d{1,2}).(\d{1,2}).(\d{4})$/);
    return (m) ? new Date(m[3], m[2]-1, m[1]) : null;
}
$(document).ready(function(){
    /*$('.time_input').bootstrapMaterialDatePicker({
        date: false,
        shortTime: false,
        format: 'HH:mm'
    });

    $('#date-format').bootstrapMaterialDatePicker({
        format: 'dddd DD MMMM YYYY - HH:mm'
    });
    $('#date-fr').bootstrapMaterialDatePicker({
        format: 'DD/MM/YYYY HH:mm',
        lang: 'fr',
        weekStart: 1, 
        cancelText : 'ANNULER',
        nowButton : true,
        switchOnClick : true
    });

    $('#date-end').bootstrapMaterialDatePicker({
        weekStart: 0, format: 'DD/MM/YYYY HH:mm'
    });
    $('#date-start').bootstrapMaterialDatePicker({
        weekStart: 0, format: 'DD/MM/YYYY HH:mm', shortTime : true
    }).on('change', function(e, date)
    {
        $('#date-end').bootstrapMaterialDatePicker('setMinDate', date);
    });

    $.material.init()*/

    $('.clockpicker').clockpicker({
        /*beforeShow: function(){
            console.log($(this));
        }*/
    });

    $('.accordion-w1, .wrapper_accordion').accordion({
        collapsible: true,
        active: false,
        heightStyle: "content",
        header: "> div > h3"
    });
    $('.pagenav input').keyup(function(event) {
        var maxVal = $(this).data("max");
        if (this.value.match(/[^0-9]/g)) {
            this.value = this.value.replace(/[^0-9]/g, '');
        }
        else if(this.value === '0'){
            this.value = 1;
        }
    });
    $('.pagenav input').blur(function(){
        var myVal = $(this).val();
        var maxVal = $(this).data("max");
        if(myVal > maxVal){
            $(this).val(maxVal);
        }
        else if(myVal === ""){
            $(this).val(1);
        }
    });
});